#pragma once

#include <string>

size_t string_actual_printed_length(const std::string& str);
